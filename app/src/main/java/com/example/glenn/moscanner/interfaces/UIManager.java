package com.example.glenn.moscanner.interfaces;

import com.example.glenn.moscanner.repository.model.Device;

public interface UIManager {

    void loadScannerListFragment();
    void loadScannerDetailsFragment(Device device);

}
