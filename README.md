# MoScanner
This is a test for an Android Developer position at Molekule.

This application scans looking for Bluetooth Low Energy devices.

I used [Ble Peripheral Simulator](https://play.google.com/store/apps/details?id=com.ble.peripheral.sim) application which can be found for free on Google Play to simulate BLE Devices in order to perform my manual tests.

The device was giving me wrong access level values despite different options were selected. The same issue happened for the Characteristic value. Although if I've got the BLE advertising data package protocol right, it should work fine.
