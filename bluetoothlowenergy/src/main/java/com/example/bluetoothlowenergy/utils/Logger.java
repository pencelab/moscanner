package com.example.bluetoothlowenergy.utils;

import android.util.Log;

public class Logger {

    private static final String TAG = "MoScannerTag";

    @SuppressWarnings("unused")
    public static void log(String message){
        Log.d(TAG, message + " : " + Thread.currentThread().getName());
    }

    @SuppressWarnings("unused")
    public static void log(Throwable error){
        Log.e(TAG, error.getMessage() + " : " + Thread.currentThread().getName());
    }

}
