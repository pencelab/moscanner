package com.example.glenn.moscanner.ui.fragments.devices;

import android.Manifest;
import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.example.glenn.moscanner.R;
import com.example.glenn.moscanner.business.ScannerViewModel;
import com.example.glenn.moscanner.business.ViewModelFactory;
import com.example.glenn.moscanner.dependencyinjection.App;
import com.example.glenn.moscanner.dependencyinjection.viewmodel.ViewModelModule;
import com.example.glenn.moscanner.interfaces.Scanner;
import com.example.glenn.moscanner.ui.events.ConnectToDeviceEvent;
import com.example.glenn.moscanner.utils.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Objects;

import javax.inject.Inject;

public class ScannerListFragment extends Fragment implements Scanner.View{

    public static final String TAG = "scanner";

    private static final int REQUEST_CODE_ACCESS_LOCATION = 1001;

    @Inject
    ViewModelFactory viewModelFactory;

    private ScannerViewModel scannerViewModel;

    private Button scanButton;
    private Button stopButton;
    private ListView devicesListView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_scanner_list, container, false);

        ((App) Objects.requireNonNull(getActivity()).getApplication()).getComponent().newViewModelComponent(new ViewModelModule()).inject(this);
        this.scannerViewModel = ViewModelProviders.of(this.getActivity(), this.viewModelFactory).get(ScannerViewModel.class);

        this.scanButton = view.findViewById(R.id.button_scan);
        this.scanButton.setOnClickListener(v -> {
            if(!this.scannerViewModel.isBluetoothActivated())
                this.requestBluetoothActivation();
            else
                this.checkScanPermissions();
        });

        this.stopButton = view.findViewById(R.id.button_stop);
        this.stopButton.setOnClickListener(v -> this.scannerViewModel.stopScanningProcess());

        this.devicesListView = view.findViewById(R.id.listView_devices);

        this.subscribeToBTScanningStateChanges();
        this.subscribeToBTDeviceFound();
        this.subscribeToBTErrorFound();

        return view;
    }

    private void subscribeToBTScanningStateChanges(){
        this.scannerViewModel.getStatus().observe(this.getViewLifecycleOwner(), state -> {
            if (state != null) {
                switch(state){
                    case IDLE:
                        this.displayIdleState();
                        break;
                    case SCANNING:
                        this.displayScanningState();
                        break;
                }
            }
        });
    }

    private void subscribeToBTDeviceFound(){
        this.scannerViewModel.getDevices().observe(this.getViewLifecycleOwner(), devices -> {
            DevicesAdapter devicesAdapter = new DevicesAdapter(this.getContext(), devices);
            this.devicesListView.setAdapter(devicesAdapter);
        });
    }

    private void subscribeToBTErrorFound(){
        this.scannerViewModel.getError().observe(this.getViewLifecycleOwner(), error -> Logger.log(new Exception(error)));
    }

    private void checkScanPermissions(){
        if(ActivityCompat.checkSelfPermission(Objects.requireNonNull(this.getActivity()), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this.getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            this.scannerViewModel.startScanningProcess();
        }else{
            if(ActivityCompat.shouldShowRequestPermissionRationale(this.getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                    && ActivityCompat.shouldShowRequestPermissionRationale(this.getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());
                builder.setMessage(getString(R.string.permission_required_text_location))
                        .setTitle(getString(R.string.permission_required_title));

                builder.setPositiveButton(getString(R.string.button_ok_text), (dialog, which) -> requestPermission());

                AlertDialog dialog = builder.create();
                dialog.show();
            }else{
                this.requestPermission();
            }
        }
    }

    private void requestBluetoothActivation(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());
        builder.setMessage(getString(R.string.enable_bluetooth))
                .setPositiveButton(getString(R.string.button_yes_text), dialogClickListener)
                .setNegativeButton(getString(R.string.button_no_text), dialogClickListener)
                .show();
    }

    private final DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
        switch (which){
            case DialogInterface.BUTTON_POSITIVE:
                dialog.dismiss();
                this.activateBluetooth();
                break;

            case DialogInterface.BUTTON_NEGATIVE:
                dialog.dismiss();
                this.showActivationRequiredMessage();
                break;
        }
    };

    private void activateBluetooth(){
        this.scannerViewModel.enableBluetooth();
    }

    private void showActivationRequiredMessage(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());
        builder.setMessage(getString(R.string.bluetooth_activation_required))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.button_ok_text), (dialog, id) -> {});
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void requestPermission(){
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        this.requestPermissions(permissions, REQUEST_CODE_ACCESS_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case REQUEST_CODE_ACCESS_LOCATION:
                if(permissions.length > 0 && grantResults.length > 0){
                    int index = -1;
                    for (int i = 0; i < permissions.length; i++) {
                        if (permissions[i].equals(Manifest.permission.ACCESS_FINE_LOCATION) || permissions[i].equals(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                            index = i;
                            break;
                        }
                    }

                    if(index >= 0 && grantResults[index] == PackageManager.PERMISSION_GRANTED){
                        this.scannerViewModel.startScanningProcess();
                    }else{
                        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());
                        builder.setMessage(getString(R.string.permission_required_text_location_denied))
                                .setTitle(getString(R.string.permission_required_title_denied));

                        builder.setPositiveButton(getString(R.string.button_ok_text), (dialog, which) -> dialog.dismiss());

                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
                break;
        }
    }

    @Override
    public void displayIdleState() {
        this.stopButton.setVisibility(View.GONE);
        this.scanButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void displayScanningState() {
        this.scanButton.setVisibility(View.GONE);
        this.stopButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        this.scannerViewModel.startObservers();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        this.scannerViewModel.stopObservers();
    }

    @Subscribe
    public void onConnectToDeviceEvent(ConnectToDeviceEvent event){
        this.scannerViewModel.stopScanningProcess();
    }
}
