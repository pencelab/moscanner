package com.example.glenn.moscanner.dependencyinjection.viewmodel;

import android.app.Application;

import com.example.bluetoothlowenergy.manager.IBLEManager;
import com.example.glenn.moscanner.business.DetailsViewModel;
import com.example.glenn.moscanner.business.ScannerViewModel;
import com.example.glenn.moscanner.business.ViewModelFactory;
import com.example.glenn.moscanner.dependencyinjection.ActivityScope;
import com.example.glenn.moscanner.interfaces.Details;
import com.example.glenn.moscanner.interfaces.Scanner;
import com.example.glenn.moscanner.repository.bluetooth.DetailsModelImpl;
import com.example.glenn.moscanner.repository.bluetooth.ScannerModelImpl;

import dagger.Module;
import dagger.Provides;
import dagger.Reusable;

@Module
public class ViewModelModule {

    @ActivityScope
    @Provides
    ViewModelFactory provideViewModelFactory(ScannerViewModel scannerViewModel, DetailsViewModel detailsViewModel){
        return new ViewModelFactory(scannerViewModel, detailsViewModel);
    }

    @Provides
    ScannerViewModel provideScannerViewModel(Scanner.Model scannerModel){
        return new ScannerViewModel(scannerModel);
    }

    @Provides
    DetailsViewModel provideDetailsViewModel(Details.Model detailsModel){
        return new DetailsViewModel(detailsModel);
    }

    @Reusable
    @Provides
    Scanner.Model provideScannerModel(IBLEManager bleManager, Application application){
        return new ScannerModelImpl(bleManager, application);
    }

    @Reusable
    @Provides
    Details.Model provideDetailsModel(IBLEManager bleManager, Application application){
        return new DetailsModelImpl(bleManager, application);
    }

}
