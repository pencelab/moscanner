package com.example.glenn.moscanner.utils;

import android.support.annotation.Nullable;

import com.example.glenn.moscanner.repository.model.Record;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RecordParser {

    private static final int DATA_TYPE_FLAGS = 0x01;
    /*private static final int DATA_TYPE_SERVICE_UUIDS_16_BIT_PARTIAL = 0x02;
    private static final int DATA_TYPE_SERVICE_UUIDS_16_BIT_COMPLETE = 0x03;
    private static final int DATA_TYPE_SERVICE_UUIDS_32_BIT_PARTIAL = 0x04;
    private static final int DATA_TYPE_SERVICE_UUIDS_32_BIT_COMPLETE = 0x05;
    private static final int DATA_TYPE_SERVICE_UUIDS_128_BIT_PARTIAL = 0x06;
    private static final int DATA_TYPE_SERVICE_UUIDS_128_BIT_COMPLETE = 0x07;
    private static final int DATA_TYPE_LOCAL_NAME_SHORT = 0x08;
    private static final int DATA_TYPE_LOCAL_NAME_COMPLETE = 0x09;
    private static final int DATA_TYPE_TX_POWER_LEVEL = 0x0A;
    private static final int DATA_TYPE_SERVICE_DATA = 0x16;*/
    private static final int DATA_TYPE_MANUFACTURER_SPECIFIC_DATA = 0xFF;

    private static final int ADV_IND = 0x00;
    private static final int ADV_DIRECT_IND  = 0x01;
    /*private static final int ADV_NONCONN_IND = 0x02;
    private static final int SCAN_REQ = 0x03;
    private static final int SCAN_RSP = 0x04;*/
    private static final int CONNECT_REQ = 0x05;
    //private static final int ADV_SCAN_IND = 0x06;

    public static List<Record> parseScanRecord(byte[] scanRecord) {
        List<Record> records = new ArrayList<>();

        int index = 0;
        while (index < scanRecord.length) {
            int length = scanRecord[index++];

            if (length == 0)
                break;

            byte type = scanRecord[index];
            if (type == 0)
                break;

            byte[] data = Arrays.copyOfRange(scanRecord, index+1, index+length);
            records.add(new Record(type, data));

            index += length;
        }

        return records;
    }

    @Nullable
    public static String getManufacturerData(List<Record> records){
        String manufacturerData = null;
        for(Record record : records){
            if(record.getType() == (byte) DATA_TYPE_MANUFACTURER_SPECIFIC_DATA){
                try {
                    manufacturerData = new String(record.getData(),"UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
            }
        }

        return manufacturerData;
    }

    public static boolean isConnectable(List<Record> records){
        boolean connectable = false;
        for(Record record : records){
            if(record.getType() == DATA_TYPE_FLAGS){
                switch(record.getData()[0]){
                    case ADV_IND:
                    case ADV_DIRECT_IND:
                    case CONNECT_REQ:
                        connectable = true;
                        break;
                }
                break;
            }
        }
        return connectable;
    }

}
