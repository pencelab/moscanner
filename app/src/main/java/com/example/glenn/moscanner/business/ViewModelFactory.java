package com.example.glenn.moscanner.business;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

public class ViewModelFactory implements ViewModelProvider.Factory {

    private final ScannerViewModel scannerViewModel;
    private final DetailsViewModel detailsViewModel;

    public ViewModelFactory(ScannerViewModel scannerViewModel, DetailsViewModel detailsViewModel) {
        this.scannerViewModel = scannerViewModel;
        this.detailsViewModel = detailsViewModel;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        ViewModel viewModel;
        if(modelClass == ScannerViewModel.class) {
            viewModel = this.scannerViewModel;
        }else if(modelClass == DetailsViewModel.class){
            viewModel = this.detailsViewModel;
        }else{
            throw new RuntimeException("ViewModel class not taken into account. Class: " + modelClass);
        }

        return (T) viewModel;
    }
}
