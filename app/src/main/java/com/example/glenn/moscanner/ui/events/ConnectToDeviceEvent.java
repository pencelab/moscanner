package com.example.glenn.moscanner.ui.events;

import com.example.glenn.moscanner.repository.model.Device;

public class ConnectToDeviceEvent {

    public final Device device;

    public ConnectToDeviceEvent(Device device){
        this.device = device;
    }

}
