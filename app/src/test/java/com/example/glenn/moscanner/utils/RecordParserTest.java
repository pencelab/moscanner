package com.example.glenn.moscanner.utils;

import com.example.glenn.moscanner.repository.model.Record;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class RecordParserTest {

    @Before
    public void setUp() {
    }

    private byte[] scanRecord = {0x02, 0x01, 0x05, 0x0C, 0x09, 0x4D, 0x79, 0x4C, 0x6F, 0x63, 0x61, 0x6C, 0x4E, 0x61, 0x6D, 0x65, 0x06, (byte) 0xFF, 0x4D, 0x61, 0x6E, 0x75, 0x66};
    private List<Record> records = RecordParser.parseScanRecord(this.scanRecord);

    @Test
    public void shouldHaveThreeElementsWhenParsingScanRecord() {
        assertEquals(3, this.records.size());
    }

    @Test
    public void shouldGetManufacturerData() {
        assertEquals("Manuf", RecordParser.getManufacturerData(this.records));
    }

    @Test
    public void shouldBeConnectable() {
        assertTrue(RecordParser.isConnectable(this.records));
    }

    @Test
    public void shouldNotBeConnectable() {
        byte[] scanRecordNotConnectable = this.scanRecord;
        scanRecordNotConnectable[2] = 0x02;
        List<Record> recordsNotConnectable = RecordParser.parseScanRecord(scanRecordNotConnectable);
        assertFalse(RecordParser.isConnectable(recordsNotConnectable));
    }

    @After
    public void tearDown() {
    }
}