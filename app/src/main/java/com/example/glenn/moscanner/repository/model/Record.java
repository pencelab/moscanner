package com.example.glenn.moscanner.repository.model;

public class Record {
    private final byte type;
    private final byte[] data;

    public Record(byte type, byte[] data) {
        this.type = type;
        this.data = data;
    }

    public byte getType() {
        return this.type;
    }

    public byte[] getData() {
        return data;
    }
}
