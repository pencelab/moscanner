package com.example.bluetoothlowenergy.manager;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.le.ScanResult;
import android.content.Context;

import io.reactivex.subjects.Subject;

public interface IBLEManager {

    enum ScanningState{
        IDLE,
        SCANNING
    }

    boolean isBluetoothActivated();
    void activateBluetooth();
    boolean isBleAvailable(Context context);
    void startScanningProcess();
    void stopScanningProcess();
    void connectToDevice(Context context, BluetoothDevice device);
    void disconnect();

    Subject<BLEManager.ScanningState> getScanningStatusSubject();
    Subject<ScanResult> getScanResultSubject();
    Subject<String> getErrorSubject();
    Subject<BluetoothGattService> getServiceSubject();
}
