package com.example.glenn.moscanner.ui.fragments.devices;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.glenn.moscanner.R;
import com.example.glenn.moscanner.repository.model.Device;
import com.example.glenn.moscanner.ui.events.ConnectToDeviceEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

class DevicesAdapter extends BaseAdapter {

    private final Context context;
    private final List<Device> devices;

    DevicesAdapter(Context context, List<Device> devices){
        this.context = context;
        this.devices = devices;
    }

    @Override
    public int getCount() {
        return this.devices.size();
    }

    @Override
    public Object getItem(int position) {
        return this.devices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        @SuppressLint({"InflateParams", "ViewHolder"})
        View view = inflater.inflate(R.layout.item_device, null);

        TextView deviceNameTextView = view.findViewById(R.id.textView_device_name);
        TextView connectableTextView = view.findViewById(R.id.textView_connectable);
        Button connectButton = view.findViewById(R.id.button_connect);
        connectButton.setOnClickListener(v -> EventBus.getDefault().post(new ConnectToDeviceEvent(this.devices.get(position))));
        deviceNameTextView.setText(this.devices.get(position).getName());

        if(this.devices.get(position).isConnectable()) {
            connectableTextView.setText(context.getString(R.string.connectable));
            connectButton.setVisibility(View.VISIBLE);
        }else{
            connectableTextView.setText(context.getString(R.string.no_connectable));
            connectButton.setVisibility(View.GONE);
        }

        return view;
    }
}
