package com.example.glenn.moscanner.ui;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.glenn.moscanner.R;
import com.example.glenn.moscanner.interfaces.UIManager;
import com.example.glenn.moscanner.repository.model.Device;
import com.example.glenn.moscanner.ui.events.ConnectToDeviceEvent;
import com.example.glenn.moscanner.ui.events.DetailsFragmentBackButtonPressedEvent;
import com.example.glenn.moscanner.ui.fragments.details.ScannerDetailsFragment;
import com.example.glenn.moscanner.ui.fragments.devices.ScannerListFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class ScannerActivity extends AppCompatActivity implements UIManager {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);

        if(savedInstanceState == null) {
            this.loadScannerListFragment();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        int count = this.getSupportFragmentManager().getBackStackEntryCount();
        if(count > 1){
            this.getSupportFragmentManager().popBackStack();
        }else{
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Subscribe
    public void onConnectToDeviceEvent(ConnectToDeviceEvent event){
        this.loadScannerDetailsFragment(event.device);
    }

    @Subscribe
    public void onDetailsFragmentBackButtonPressed(DetailsFragmentBackButtonPressedEvent event){
        this.onBackPressed();
    }

    @Override
    public void loadScannerListFragment() {
        ScannerListFragment scannerListFragment = new ScannerListFragment();
        this.openFragment(scannerListFragment, ScannerListFragment.TAG);
    }

    @Override
    public void loadScannerDetailsFragment(Device device) {
        ScannerDetailsFragment scannerDetailsFragment = new ScannerDetailsFragment();
        this.getIntent().putExtra(ScannerDetailsFragment.DEVICE_KEY, device);
        this.openFragment(scannerDetailsFragment, ScannerDetailsFragment.TAG);
    }

    private void openFragment(Fragment fragment, String tag){
        this.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, fragment)
                .addToBackStack(tag)
                .commit();
    }
}
