package com.example.glenn.moscanner.interfaces;

import android.bluetooth.le.ScanResult;

import com.example.bluetoothlowenergy.manager.IBLEManager;

import io.reactivex.subjects.Subject;

public interface Scanner {

    interface View{
        void displayIdleState();
        void displayScanningState();
    }

    interface ViewModel{
        void startObservers();
        void stopObservers();
        void startScanningProcess();
        void stopScanningProcess();
        void enableBluetooth();
        boolean isBluetoothActivated();
        boolean isBleAvailable();
    }

    interface Model{
        void scan();
        void stop();
        boolean isBluetoothActivated();
        void enableBluetooth();
        boolean isBleAvailable();
        Subject<IBLEManager.ScanningState> getScanningStatusSubject();
        Subject<ScanResult> getScanResultSubject();
        Subject<String> getErrorSubject();
    }
}
