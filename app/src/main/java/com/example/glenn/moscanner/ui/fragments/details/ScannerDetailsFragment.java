package com.example.glenn.moscanner.ui.fragments.details;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.glenn.moscanner.R;
import com.example.glenn.moscanner.business.DetailsViewModel;
import com.example.glenn.moscanner.business.ViewModelFactory;
import com.example.glenn.moscanner.dependencyinjection.App;
import com.example.glenn.moscanner.dependencyinjection.viewmodel.ViewModelModule;
import com.example.glenn.moscanner.repository.model.Device;
import com.example.glenn.moscanner.ui.events.DetailsFragmentBackButtonPressedEvent;
import com.example.glenn.moscanner.utils.Logger;

import org.greenrobot.eventbus.EventBus;
import java.util.Objects;

import javax.inject.Inject;

public class ScannerDetailsFragment extends Fragment {

    public static final String TAG = "details";

    public static final String DEVICE_KEY = "device";

    @Inject
    ViewModelFactory viewModelFactory;

    private DetailsViewModel detailsViewModel;

    private LinearLayout loadingServicesLayout;
    private ListView servicesListView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_scanner_details, container, false);

        ((App) Objects.requireNonNull(getActivity()).getApplication()).getComponent().newViewModelComponent(new ViewModelModule()).inject(this);
        this.detailsViewModel = ViewModelProviders.of(this.getActivity(), this.viewModelFactory).get(DetailsViewModel.class);

        Device device = (Device) Objects.requireNonNull(getActivity()).getIntent().getSerializableExtra(DEVICE_KEY);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(v -> EventBus.getDefault().post(new DetailsFragmentBackButtonPressedEvent()));

        TextView titleTextView = view.findViewById(R.id.textView_title);
        this.loadingServicesLayout = view.findViewById(R.id.layout_loading_services);
        TextView loadingServicesTextView = view.findViewById(R.id.textView_loading_services);
        this.servicesListView = view.findViewById(R.id.listView_services);

        titleTextView.setText(device.getBluetoothDevice().getName());
        loadingServicesTextView.setText(this.getString(R.string.loading_services, device.getName()));

        this.subscribeToBTServiceFound(device);

        if(savedInstanceState == null) {
            this.detailsViewModel.connectToDevice(device.getBluetoothDevice());
        }

        return view;
    }

    private void subscribeToBTServiceFound(@NonNull final Device device){
        this.detailsViewModel.getServices().observe(this.getViewLifecycleOwner(), services -> {
            if(services != null) {
                this.loadingServicesLayout.setVisibility(View.GONE);
                this.servicesListView.setVisibility(View.VISIBLE);
                ServicesAdapter adapter = new ServicesAdapter(Objects.requireNonNull(getContext()), device.getBluetoothDevice().getName(), device.getManufacturerData(), services);
                servicesListView.setAdapter(adapter);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.detailsViewModel.disconnect();
    }

    @Override
    public void onStart() {
        super.onStart();
        this.detailsViewModel.startObservers();
    }

    @Override
    public void onStop() {
        super.onStop();
        this.detailsViewModel.stopObservers();
    }
}
