package com.example.glenn.moscanner.repository.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattService;
import android.content.Context;

import com.example.bluetoothlowenergy.manager.IBLEManager;
import com.example.glenn.moscanner.interfaces.Details;

import io.reactivex.subjects.Subject;

public class DetailsModelImpl implements Details.Model {

    private final IBLEManager bleManager;
    private final Context context;

    public DetailsModelImpl(IBLEManager bleManager, Context context){
        this.bleManager = bleManager;
        this.context = context;
    }

    @Override
    public void connectToDevice(BluetoothDevice device) {
        this.bleManager.connectToDevice(this.context, device);
    }

    @Override
    public void disconnect() {
        this.bleManager.disconnect();
    }

    @Override
    public Subject<BluetoothGattService> getServiceSubject() {
        return this.bleManager.getServiceSubject();
    }
}
