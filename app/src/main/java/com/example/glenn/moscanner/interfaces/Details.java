package com.example.glenn.moscanner.interfaces;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattService;

import io.reactivex.subjects.Subject;

public interface Details {

    interface ViewModel{
        void startObservers();
        void stopObservers();
        void connectToDevice(BluetoothDevice device);
        void disconnect();
    }

    interface Model{
        void connectToDevice(BluetoothDevice device);
        void disconnect();
        Subject<BluetoothGattService> getServiceSubject();
    }

}
