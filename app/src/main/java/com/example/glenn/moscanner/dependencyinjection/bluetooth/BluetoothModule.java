package com.example.glenn.moscanner.dependencyinjection.bluetooth;

import android.app.Application;

import com.example.bluetoothlowenergy.manager.BLEManager;
import com.example.bluetoothlowenergy.manager.IBLEManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class BluetoothModule {

    @Provides
    @Singleton
    IBLEManager provideBLEManager(Application application){
        return new BLEManager(application);
    }

}
