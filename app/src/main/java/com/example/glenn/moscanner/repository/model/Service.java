package com.example.glenn.moscanner.repository.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

public class Service {

    @NonNull
    private final String uuid;
    @NonNull
    private final List<Characteristic> characteristics;

    public Service(@NonNull String uuid, @NonNull List<Characteristic> characteristics){
        this.uuid = uuid;
        this.characteristics = characteristics;
    }

    @NonNull
    public String getUuid() {
        return this.uuid;
    }

    @NonNull
    public List<Characteristic> getCharacteristics() {
        return this.characteristics;
    }

    public static class Characteristic {
        @NonNull
        private final String UUID;
        private final boolean read;
        private final boolean write;
        @Nullable
        private final String value;

        public Characteristic(@NonNull String UUID, boolean read, boolean write, @Nullable String value){
            this.UUID = UUID;
            this.read = read;
            this.write = write;
            this.value = value;
        }

        @NonNull
        public String getUUID() {
            return this.UUID;
        }

        public boolean isReadAvailable() {
            return this.read;
        }

        public boolean isWriteAvailable() {
            return this.write;
        }

        @Nullable
        public String getValue() {
            return this.value;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (!(obj instanceof Service))
            return false;
        if (obj == this)
            return true;

        return this.uuid.equals(((Service) obj).uuid);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + this.uuid.hashCode();
        return hash;
    }
}