package com.example.glenn.moscanner.business;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;

import com.example.bluetoothlowenergy.manager.IBLEManager;
import com.example.glenn.moscanner.interfaces.Scanner;
import com.example.glenn.moscanner.repository.model.Device;
import com.example.glenn.moscanner.repository.model.Record;
import com.example.glenn.moscanner.utils.RecordParser;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ScannerViewModel extends ViewModel implements Scanner.ViewModel{

    private CompositeDisposable disposables;

    private final Scanner.Model scannerModel;

    private final List<Device> devices = new ArrayList<>();
    private final MutableLiveData<List<Device>> ldDevices = new MutableLiveData<>();
    private final MutableLiveData<IBLEManager.ScanningState> status = new MutableLiveData<>();
    private final MutableLiveData<String> error = new MutableLiveData<>();

    public ScannerViewModel(Scanner.Model scannerModel){
        this.scannerModel = scannerModel;
    }

    @Override
    public void startObservers(){
        if(this.disposables == null)
            this.disposables = new CompositeDisposable();

        this.subscribeToScanResult();
        this.subscribeToStatus();
        this.subscribeToError();
    }

    @Override
    public void stopObservers() {
        if(this.disposables != null) {
            this.disposables.clear();
        }

        this.disposables = null;
    }

    @Override
    public void startScanningProcess() {
        this.scannerModel.scan();
    }

    @Override
    public void stopScanningProcess() {
        this.scannerModel.stop();
    }

    @Override
    public boolean isBluetoothActivated() {
        return this.scannerModel.isBluetoothActivated();
    }

    @Override
    public boolean isBleAvailable() {
        return this.scannerModel.isBleAvailable();
    }

    @Override
    public void enableBluetooth() {
        this.scannerModel.enableBluetooth();
    }

    private void subscribeToScanResult(){
        this.disposables.add(
                this.scannerModel.getScanResultSubject()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(scanResult -> {
                            Device device = this.getDevice(scanResult);
                            if(device != null && !this.devices.contains(device)) {
                                this.devices.add(device);
                                this.ldDevices.setValue(this.devices);
                            }
                        })
        );
    }

    private void subscribeToStatus(){
        this.disposables.add(
                this.scannerModel.getScanningStatusSubject()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(this.status::setValue)
        );
    }

    private void subscribeToError(){
        this.disposables.add(
                this.scannerModel.getErrorSubject()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(this.error::setValue)
        );
    }

    private Device getDevice(ScanResult result) {
        ScanRecord scanRecord = result.getScanRecord();
        if(scanRecord == null || result.getDevice().getName() == null)
            return null;

        List<Record> records = RecordParser.parseScanRecord(scanRecord.getBytes());
        String manufacturerData = RecordParser.getManufacturerData(records);
        boolean connectable = RecordParser.isConnectable(records);
        return new Device(result.getDevice().getName(), result.getDevice(), connectable, manufacturerData);
    }

    public LiveData<List<Device>> getDevices(){
        return this.ldDevices;
    }

    public LiveData<IBLEManager.ScanningState> getStatus(){
        return this.status;
    }

    public LiveData<String> getError(){
        return this.error;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        this.stopScanningProcess();
    }

}
