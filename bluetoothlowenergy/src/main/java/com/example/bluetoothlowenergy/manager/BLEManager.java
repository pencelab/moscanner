package com.example.bluetoothlowenergy.manager;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.pm.PackageManager;

import com.example.bluetoothlowenergy.utils.Logger;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

public class BLEManager implements IBLEManager{

    private final BluetoothAdapter bluetoothAdapter;
    private BluetoothLeScanner bluetoothLeScanner = null;
    private ScanCallback scanCallback = null;

    private final Subject<ScanningState> scanningStateSubject = BehaviorSubject.create();
    private final Subject<ScanResult> scanResultSubject = PublishSubject.create();
    private final Subject<String> errorSubject = PublishSubject.create();
    private final Subject<BluetoothGattService> serviceSubject = PublishSubject.create();

    private boolean scanning = false;

    private BluetoothGatt currentBluetoothGatt;

    public BLEManager(Context context){
        this.scanningStateSubject.onNext(ScanningState.IDLE);
        BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        this.bluetoothAdapter = bluetoothManager.getAdapter();
    }

    @Override
    public boolean isBluetoothActivated() {
        return this.bluetoothAdapter.isEnabled();
    }

    @Override
    public void activateBluetooth() {
        this.bluetoothAdapter.enable();
    }

    public boolean isBleAvailable(Context context){
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
    }

    @Override
    public void startScanningProcess() {
        if(this.scanning)
            return;

        List<ScanFilter> filters = new ArrayList<>();
        ScanSettings settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
                .build();

        this.scanCallback = new BleScanCallback();
        this.bluetoothLeScanner = this.bluetoothAdapter.getBluetoothLeScanner();
        this.bluetoothLeScanner.startScan(filters, settings, this.scanCallback);
        this.scanning = true;
        this.scanningStateSubject.onNext(ScanningState.SCANNING);
    }

    @Override
    public void stopScanningProcess() {
        if(this.scanning && this.bluetoothLeScanner != null)
            this.bluetoothLeScanner.stopScan(this.scanCallback);

        this.bluetoothLeScanner = null;
        this.scanCallback = null;
        this.scanning = false;
        this.scanningStateSubject.onNext(ScanningState.IDLE);
    }

    @Override
    public Subject<ScanningState> getScanningStatusSubject() {
        return this.scanningStateSubject;
    }

    @Override
    public Subject<ScanResult> getScanResultSubject() {
        return this.scanResultSubject;
    }

    @Override
    public Subject<String> getErrorSubject() {
        return this.errorSubject;
    }

    @Override
    public Subject<BluetoothGattService> getServiceSubject() {
        return this.serviceSubject;
    }

    @Override
    public void connectToDevice(Context context, BluetoothDevice device) {
        GattClientCallback gattClientCallback = new GattClientCallback();
        device.connectGatt(context, false, gattClientCallback);
    }

    @Override
    public void disconnect() {
        if(this.currentBluetoothGatt != null)
            this.currentBluetoothGatt.close();

        this.currentBluetoothGatt = null;
    }

    private class BleScanCallback extends ScanCallback {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            scanResultSubject.onNext(result);
        }
        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            for (ScanResult result : results) {
                scanResultSubject.onNext(result);
            }
        }
        @Override
        public void onScanFailed(int errorCode) {
            Logger.log(new Throwable("BLE Scan returned with error code: " + errorCode));
        }
    }

    private class GattClientCallback extends BluetoothGattCallback {

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                currentBluetoothGatt = gatt;
                gatt.discoverServices();
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                for(BluetoothGattService bluetoothGattService : gatt.getServices()) {
                    serviceSubject.onNext(bluetoothGattService);
                }
            }
        }
    }
}
