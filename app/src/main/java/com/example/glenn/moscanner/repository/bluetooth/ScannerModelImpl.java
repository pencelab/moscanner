package com.example.glenn.moscanner.repository.bluetooth;

import android.bluetooth.le.ScanResult;
import android.content.Context;

import com.example.bluetoothlowenergy.manager.IBLEManager;
import com.example.glenn.moscanner.interfaces.Scanner;

import io.reactivex.subjects.Subject;

public class ScannerModelImpl implements Scanner.Model {

    private final IBLEManager bleManager;
    private final Context context;

    public ScannerModelImpl(IBLEManager bleManager, Context context){
        this.bleManager = bleManager;
        this.context = context;
    }

    @Override
    public void scan() {
        this.bleManager.startScanningProcess();
    }

    @Override
    public void stop() {
        this.bleManager.stopScanningProcess();
    }

    @Override
    public boolean isBluetoothActivated() {
        return this.bleManager.isBluetoothActivated();
    }

    @Override
    public void enableBluetooth() {
        this.bleManager.activateBluetooth();
    }

    @Override
    public boolean isBleAvailable() {
        return this.bleManager.isBleAvailable(this.context);
    }

    @Override
    public Subject<IBLEManager.ScanningState> getScanningStatusSubject() {
        return this.bleManager.getScanningStatusSubject();
    }

    @Override
    public Subject<ScanResult> getScanResultSubject() {
        return this.bleManager.getScanResultSubject();
    }

    @Override
    public Subject<String> getErrorSubject() {
        return this.bleManager.getErrorSubject();
    }
}
