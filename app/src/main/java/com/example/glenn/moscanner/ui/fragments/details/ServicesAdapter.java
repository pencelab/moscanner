package com.example.glenn.moscanner.ui.fragments.details;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.glenn.moscanner.R;
import com.example.glenn.moscanner.repository.model.Service;

import java.util.List;

class ServicesAdapter extends ArrayAdapter {

    @NonNull
    private final Context context;
    @NonNull
    private final String localName;
    @Nullable
    private final String manufacturerData;
    @NonNull
    private final List<Service> services;

    @SuppressWarnings("unchecked")
    ServicesAdapter(@NonNull Context context, @NonNull String localName, @Nullable String manufacturerData, @NonNull List<Service> services){
        super(context, R.layout.item_service, services);
        this.context = context;
        this.localName = localName;
        this.manufacturerData = manufacturerData;
        this.services = services;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        @SuppressLint({"ViewHolder", "InflateParams"})
        View view = inflater.inflate(R.layout.item_service, null);

        TextView localNameTextView = view.findViewById(R.id.textView_local_name);
        TextView manufacturerDataTextView = view.findViewById(R.id.textView_manufacturer_data);
        TextView uuidTextView = view.findViewById(R.id.textView_uuid);
        LinearLayout characteristicsLayout = view.findViewById(R.id.layout_characteristics);

        localNameTextView.setText(this.context.getString(R.string.local_name, this.localName));
        manufacturerDataTextView.setText(this.context.getString(R.string.manufacturer_data, this.manufacturerData));
        uuidTextView.setText(this.context.getString(R.string.uuid, this.services.get(position).getUuid()));
        for (Service.Characteristic characteristic : this.services.get(position).getCharacteristics()) {
            characteristicsLayout.addView(this.getCharacteristicInflatedView(inflater, characteristic));
        }

        return view;
    }

    private View getCharacteristicInflatedView(LayoutInflater inflater, Service.Characteristic characteristic){
        @SuppressLint({"InflateParams", "ViewHolder"})
        View child = inflater.inflate(R.layout.item_characteristic, null);

        TextView characteristicUuidTextView = child.findViewById(R.id.textView_uuid);
        TextView accessLevelTextView = child.findViewById(R.id.textView_access_level);
        TextView valueTextView = child.findViewById(R.id.textView_value);

        characteristicUuidTextView.setText(characteristic.getUUID());

        final String accessLevel;
        final String read = this.context.getString(R.string.read);
        final String write = this.context.getString(R.string.write);
        if(characteristic.isReadAvailable() && characteristic.isWriteAvailable()){
            accessLevel = this.context.getString(R.string.separator, read, write);
        }else if(characteristic.isReadAvailable()){
            accessLevel = read;
        }else if(characteristic.isWriteAvailable()){
            accessLevel = write;
        }else{
            accessLevel = this.context.getString(R.string.no_permission);
        }

        accessLevelTextView.setText(accessLevel);
        if(characteristic.getValue() != null)
            valueTextView.setText(characteristic.getValue());

        return child;
    }
}
