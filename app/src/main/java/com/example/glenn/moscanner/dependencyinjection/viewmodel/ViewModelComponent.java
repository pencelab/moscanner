package com.example.glenn.moscanner.dependencyinjection.viewmodel;

import com.example.glenn.moscanner.dependencyinjection.ActivityScope;
import com.example.glenn.moscanner.ui.fragments.details.ScannerDetailsFragment;
import com.example.glenn.moscanner.ui.fragments.devices.ScannerListFragment;

import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = ViewModelModule.class)
public interface ViewModelComponent {
    void inject(ScannerListFragment target);
    void inject(ScannerDetailsFragment target);
}
