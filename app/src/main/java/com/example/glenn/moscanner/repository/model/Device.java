package com.example.glenn.moscanner.repository.model;

import android.bluetooth.BluetoothDevice;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.Serializable;

public class Device implements Serializable {
    @NonNull
    private final String name;
    @NonNull
    private final BluetoothDevice device;
    private final boolean connectable;
    @Nullable
    private final String manufacturerData;

    public Device(@NonNull String name, @NonNull BluetoothDevice device, boolean connectable, @Nullable String manufacturerData) {
        this.name = name;
        this.device = device;
        this.connectable = connectable;
        this.manufacturerData = manufacturerData;
    }

    @NonNull
    public String getName() {
        return this.name;
    }

    @NonNull
    public BluetoothDevice getBluetoothDevice() {
        return this.device;
    }

    public boolean isConnectable() {
        return this.connectable;
    }

    @NonNull
    public String getManufacturerData() {
        if(this.manufacturerData != null)
            return this.manufacturerData;
        else
            return "";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (!(obj instanceof Device))
            return false;
        if (obj == this)
            return true;

        return this.name.equals(((Device) obj).name);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + this.name.hashCode();
        return hash;
    }
}

