package com.example.glenn.moscanner.dependencyinjection.application;

import com.example.glenn.moscanner.dependencyinjection.bluetooth.BluetoothModule;
import com.example.glenn.moscanner.dependencyinjection.viewmodel.ViewModelComponent;
import com.example.glenn.moscanner.dependencyinjection.viewmodel.ViewModelModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, BluetoothModule.class})
public interface ApplicationComponent {
    ViewModelComponent newViewModelComponent(ViewModelModule viewModelModule);
}
