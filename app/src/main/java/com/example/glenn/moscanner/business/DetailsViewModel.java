package com.example.glenn.moscanner.business;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.support.annotation.NonNull;

import com.example.glenn.moscanner.interfaces.Details;
import com.example.glenn.moscanner.repository.model.Service;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class DetailsViewModel extends ViewModel implements Details.ViewModel{

    private final Details.Model detailsModel;

    private CompositeDisposable disposables;

    private List<Service> services = new ArrayList<>();
    private MutableLiveData<List<Service>> ldServices = new MutableLiveData<>();

    public DetailsViewModel(Details.Model detailsModel){
        this.detailsModel = detailsModel;
    }

    @Override
    public void startObservers(){
        if(this.disposables == null)
            this.disposables = new CompositeDisposable();

        this.subscribeToBluetoothGattService();
    }

    @Override
    public void stopObservers(){
        if(this.disposables != null)
            this.disposables.clear();

        this.disposables = null;
    }

    @Override
    public void connectToDevice(BluetoothDevice device) {
        this.services.clear();
        this.ldServices.setValue(null);
        this.detailsModel.connectToDevice(device);
    }

    @Override
    public void disconnect() {
        this.detailsModel.disconnect();
    }

    public LiveData<List<Service>> getServices(){
        return this.ldServices;
    }

    private void subscribeToBluetoothGattService(){
        this.disposables.add(
                this.detailsModel.getServiceSubject()
                        .subscribeOn(Schedulers.io())
                        .map(this::extractServiceFromBTGattService)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(service -> {
                            if(!this.services.contains(service)) {
                                this.services.add(service);
                                this.ldServices.setValue(this.services);
                            }
                        })
        );
    }

    @NonNull
    private Service extractServiceFromBTGattService(BluetoothGattService bluetoothGattService){
        String serviceUUID = bluetoothGattService.getUuid().toString();
        List<Service.Characteristic> characteristics = new ArrayList<>();

        for(BluetoothGattCharacteristic bluetoothGattCharacteristic : bluetoothGattService.getCharacteristics()){
            boolean read = false;
            boolean write = false;
            int permissions = bluetoothGattCharacteristic.getPermissions();

            switch(permissions){
                case BluetoothGattCharacteristic.PERMISSION_READ:
                    read = true;
                    break;
                case BluetoothGattCharacteristic.PERMISSION_WRITE:
                    write = true;
                    break;
                case BluetoothGattCharacteristic.PERMISSION_READ | BluetoothGattCharacteristic.PERMISSION_WRITE:
                    read = true;
                    write = true;
                    break;
            }

            String characteristicUUID = bluetoothGattCharacteristic.getUuid().toString();
            String value = bluetoothGattCharacteristic.getStringValue(0);
            characteristics.add(new Service.Characteristic(characteristicUUID, read, write, value));
        }

        return new Service(serviceUUID, characteristics);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        this.disconnect();
    }
}
