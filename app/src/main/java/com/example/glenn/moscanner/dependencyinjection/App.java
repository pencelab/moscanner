package com.example.glenn.moscanner.dependencyinjection;

import android.app.Application;

import com.example.glenn.moscanner.dependencyinjection.application.ApplicationComponent;
import com.example.glenn.moscanner.dependencyinjection.application.ApplicationModule;
import com.example.glenn.moscanner.dependencyinjection.application.DaggerApplicationComponent;

public class App extends Application {

    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        this.component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                            .build();
    }

    public ApplicationComponent getComponent(){
        return this.component;
    }
}
